# Components

This repository contains a number of hardened components for GitLab CI.
They were originally based on GitLab's AutoDevOps but have been reworked to improve security.

## Common Inputs

Component inputs will vary depending on the job, however we try to keep them as uniform as possible.
The default values may change to match the job, so they aren't always listed here.

| Name                | Default               | Usage                                                                           |
|---------------------|-----------------------|---------------------------------------------------------------------------------|
| `project`           |                       | Scope the job to a sub-path. Useful for mono-repos.                             |
| `name`              |                       | Override the name of the job.                                                   |
| `stage`             |                       | Override the stage of the job.                                                  |
| `image`             |                       | Override the container image of the job.                                        |
